package com.letak.printing.product;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.letak.printing.product.config.MyApplicationContextInitializer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCaching
@EntityScan(basePackageClasses = {
        com.letak.printing.domain_model.Package.class, 
        com.letak.printing.product.domain.Package.class
        })
public class ProductServiceApplication {

	public static void main(String[] args) {

		SpringApplicationBuilder builder = new SpringApplicationBuilder(ProductServiceApplication.class);

		builder.initializers(new MyApplicationContextInitializer());

	    builder.run(args);
	}

}
