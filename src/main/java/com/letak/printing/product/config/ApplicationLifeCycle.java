package com.letak.printing.product.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

@Component
public class ApplicationLifeCycle implements SmartLifecycle
{
    protected Log logger = LogFactory.getLog(ApplicationLifeCycle.class);
    
    private boolean running = false;

    @Override
    public boolean isRunning()
    {
        return running;
    }

    @Override
    public void start()
    {
        running = true;
        logger.info("Lifecycle start");

        //myDatabase.create();

        running = false;
    }

    @Override
    public void stop()
    {
        logger.info("Lifecycle stop");
    }

    /**
     * Returning Integer.MAX_VALUE means we will be
     * the last bean to be called on startup and
     * the first one to be called on shutdown.
     */
    @Override
    public int getPhase()
    {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isAutoStartup()
    {
        return true;
    }

    @Override
    public void stop(Runnable callback)
    {
        logger.info("Lifecycle stop with callback");
        callback.run();
    }

}
