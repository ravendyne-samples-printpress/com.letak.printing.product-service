package com.letak.printing.product.domain;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.letak.printing.domain_model.entity.PrintPress;
import com.letak.printing.domain_model.entity.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {

	@Cacheable("productByPrintpressCache")
	List<Product> findByPrintPress(PrintPress printpress);

	// need to re-declare these methods in order to add CacheEvict annotations
	// so that query-by-category cache is updated on insert/delete
	// when a product is inserted/deleted we need to invalidate cache entry for the product's category
	
	// evict entries from the named cache where printpress.id == printpress.id from the result-set (#result?)
	@CacheEvict(cacheNames = "productByPrintpressCache", key = "#result?.printPress.id")
	<S extends Product> S save(S product);

	// evict entries from the named cache where printpress.id == printpress.id from the first (#p0) method's parameter
	@CacheEvict(cacheNames = "productByPrintpressCache", key = "#p0.printPress.id")
	void delete(Product product);
}
