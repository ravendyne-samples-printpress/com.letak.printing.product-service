package com.letak.printing.product.domain;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.letak.printing.domain_model.entity.ProductAttributeValue;

public interface ProductAttributeValueRepository extends PagingAndSortingRepository<ProductAttributeValue, Integer> {

}
