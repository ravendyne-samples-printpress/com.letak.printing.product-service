package com.letak.printing.product.domain;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.letak.printing.domain_model.entity.PrintPress;

public interface PrintPressRepository extends PagingAndSortingRepository<PrintPress, Integer> {

    List<PrintPress> findByUuid(String uuid);

}
