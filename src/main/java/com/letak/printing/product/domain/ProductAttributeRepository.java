package com.letak.printing.product.domain;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.letak.printing.domain_model.entity.ProductAttribute;

public interface ProductAttributeRepository extends PagingAndSortingRepository<ProductAttribute, Integer> {

}
