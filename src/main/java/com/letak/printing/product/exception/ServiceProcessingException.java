package com.letak.printing.product.exception;

public class ServiceProcessingException extends RuntimeException {

    private static final long serialVersionUID = 4694276551399518303L;

    public static final int ID_NOT_FOUND = 1;
    public static final int ID_BAD_REQUEST = 2;

	int errCode;

	public ServiceProcessingException(int errCode, String msg) {
		super(msg);
		this.errCode = errCode;
	}
}
