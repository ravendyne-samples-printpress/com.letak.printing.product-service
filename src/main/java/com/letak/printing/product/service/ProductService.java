package com.letak.printing.product.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.letak.printing.domain_model.dto.product.ProductBaseDto;
import com.letak.printing.domain_model.dto.product.ProductWithAttributesDto;
import com.letak.printing.domain_model.entity.PrintPress;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductAttribute;
import com.letak.printing.domain_model.entity.ProductAttributeValue;
import com.letak.printing.product.domain.PrintPressRepository;
import com.letak.printing.product.domain.ProductAttributeRepository;
import com.letak.printing.product.domain.ProductAttributeValueRepository;
import com.letak.printing.product.domain.ProductRepository;
import com.letak.printing.product.exception.ServiceProcessingException;

@RestController
public class ProductService {
    
    @Autowired
    PrintPressRepository printPressRepository;

	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	ProductAttributeRepository productAttributeRepository;
	
	@Autowired
	ProductAttributeValueRepository productAttributeValueRepository;

	@RequestMapping("/products/{uuid}")
	List<ProductWithAttributesDto> getProductIds(@PathVariable("uuid") String uuid) {
	    
	    List<PrintPress> printPressList = printPressRepository.findByUuid(uuid);
	    
        List<ProductWithAttributesDto> products = new ArrayList<ProductWithAttributesDto>();

        if( printPressList.isEmpty() ) {
	        return products;
	    }
	    
	    PrintPress printPress = printPressList.get(0);

		List<Product> productList = productRepo.findByPrintPress(printPress);
		
		for(Product product : productList) {
		    products.add(new ProductWithAttributesDto(product));
		}

		return products;
	}

	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	ProductWithAttributesDto getProduct(@PathVariable("id") int id) {

		Optional<Product> prod = productRepo.findById(id);

		if ( ! prod.isPresent() ) {

			throw new ServiceProcessingException(ServiceProcessingException.ID_NOT_FOUND, "No product for id " + id);
		}

		return new ProductWithAttributesDto(prod.get());
	}
    
    @RequestMapping(value="/product/{id}", method = RequestMethod.PUT)
    ProductBaseDto updateProduct(@PathVariable("id") int id, @RequestBody ProductBaseDto product) {
        
        // First fetch an existing product and then modify it. 
    	Optional<Product> existingProductOpt = productRepo.findById(id); 

        if ( ! existingProductOpt.isPresent() ) {

            String errMsg = "Product with code '" + id + "' not found";
            throw new ServiceProcessingException(ServiceProcessingException.ID_NOT_FOUND, errMsg);
        }
        
        // Now update it back
        // TODO move this to DTO in method updateEntity()
        Product existingProduct = existingProductOpt.get();
        existingProduct.setName(product.getName());
        existingProduct.setIcon(product.getIcon());
        existingProduct.setDescription(product.getDescription());
        existingProduct.setDisplayOrder(product.getDisplayOrder());
        Product savedProduct = productRepo.save(existingProductOpt.get()) ;
        
        // Return the updated product  
        return new ProductBaseDto(savedProduct);         
    }
    
    @RequestMapping(value="/product", method = RequestMethod.POST)
    ProductBaseDto insertProduct(@RequestBody ProductBaseDto product) {
        
        List<PrintPress> printPressList = printPressRepository.findByUuid(product.getPrintPress().getUuid());
        
        if( printPressList.isEmpty() ) {

            String errMsg = "PrintPress with uuid '" + product.getPrintPress().getUuid() + "' not found";
            throw new ServiceProcessingException(ServiceProcessingException.ID_NOT_FOUND, errMsg);
        }
        
        PrintPress printPress = printPressList.get(0);
        Product newProduct = ProductBaseDto.createEntity(product);
        newProduct.setPrintPress(printPress);
        
        Product savedProduct = productRepo.save(newProduct) ;

        return new ProductBaseDto(savedProduct);        
    }
    
    @RequestMapping(value="/product/{id}", method = RequestMethod.DELETE)
    ProductBaseDto deleteProduct(@PathVariable("id") int id) {
        
        // First fetch an existing product and then delete it. 
    	Optional<Product> existingProductOpt = productRepo.findById(id);

    	if ( ! existingProductOpt.isPresent() ) {

    		String errMsg = "Product with code '" + id + "' not found";
            throw new ServiceProcessingException(ServiceProcessingException.ID_NOT_FOUND, errMsg);
        }
    	
    	Product existingProduct = existingProductOpt.get();
    	
    	for( ProductAttribute attr: existingProduct.getProductAttributes() ) {
    	    for( ProductAttributeValue value : attr.getProductAttributeValues() ) {
    	        productAttributeValueRepository.delete(value);
    	    }
    	    attr.getProductAttributeValues().clear();
    	    productAttributeRepository.delete(attr);
    	}
    	existingProduct.getProductAttributes().clear();
        productRepo.delete(existingProduct);

        // Return the deleted product 
        return new ProductBaseDto(existingProduct);
    }
}
