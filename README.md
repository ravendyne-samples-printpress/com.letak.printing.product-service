## PrintPress Product Microservice

Implements product related operations.

### Dependencies

- [domain model](https://gitlab.com/ravendyne-samples-printpress/com.letak.printing.domain-model)
